package cours.m2gl.jee.api.hospital.service;

import cours.m2gl.jee.api.hospital.dao.ServiceRepository;
import cours.m2gl.jee.api.hospital.model.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@org.springframework.stereotype.Service
public class ServiceService implements IServiceService {
    @Autowired
    private ServiceRepository serviceRepository;
    @Override
    public Service findById(int id) {
        return serviceRepository.getOne(id);
    }

    //Ajout d'un sevice
    @Override
    public Service add(Service service) {
        try {
            serviceRepository.save(service);
            return service;
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            return null;
        }
    }

    //Suppression
    @Override
    public void deleteService(int id) {
        try {
           serviceRepository.deleteById(id);

        }catch (Exception e)
        {
            System.out.println(e.getMessage());

        }
    }

    //Modification
    @Override
    public Service UpdateServcie(Service service) {
        try {
            serviceRepository.save(service);
            return service;
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            return null;
        }
    }

    @Override
    public List<Service> findAll() {
        return serviceRepository.findAll();
    }
}
