package cours.m2gl.jee.api.hospital.controller;

import cours.m2gl.jee.api.hospital.model.Service;
import cours.m2gl.jee.api.hospital.service.IServiceService;
import cours.m2gl.jee.api.hospital.service.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@EnableAutoConfiguration
@RequestMapping(value = "/service")
public class ServiceController {
    @Autowired
    private IServiceService serviceService;


    //Ajout d'un service
    @PreAuthorize("hasAuthority('ROLE_SECRETAIRE') or hasAuthority('ROLE_MEDECIN')")
    @PostMapping(value = "/add" , consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Service addService(@RequestBody Service service){
        return serviceService.add(service);
    }

    //Liste de tous les utilisateurs
    @PreAuthorize("hasAuthority('ROLE_MEDECIN')")
    @GetMapping("/all")
    public @ResponseBody List<Service> list(){
        return serviceService.findAll();
    }

    //Mise à jour des informations
    @PreAuthorize("hasAuthority('ROLE_SECRETAIRE') or hasAuthority('ROLE_MEDECIN')")
    @PostMapping(value = "/update" , consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Service UpdateService(@RequestBody Service service){
        return serviceService.add(service);
    }

    //Suppression
    @PreAuthorize("hasAuthority('ROLE_SECRETAIRE') or hasAuthority('ROLE_MEDECIN')")
    @PostMapping(value = "/delete" , consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<Service> DeleteService(@RequestBody Service service){
        serviceService.deleteService(service.getId());
        return serviceService.findAll();
    }
}
