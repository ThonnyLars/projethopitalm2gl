package cours.m2gl.jee.api.hospital.service;

import cours.m2gl.jee.api.hospital.model.Service;

import java.util.List;

public interface IServiceService {

    //Retrouver un service par son id
    public Service findById(int id);

    //Ajout d'un service
    public Service add(Service service);

    //suppression d'un service
    public void deleteService(int id);

    //Mettre à jour les informations d'un service
    public Service UpdateServcie(Service service);

    //Liste de tous les services
    public List<Service> findAll();
}
