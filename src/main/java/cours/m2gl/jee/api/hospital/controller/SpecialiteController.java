package cours.m2gl.jee.api.hospital.controller;

import cours.m2gl.jee.api.hospital.model.Service;
import cours.m2gl.jee.api.hospital.model.Specialite;
import cours.m2gl.jee.api.hospital.service.IServiceService;
import cours.m2gl.jee.api.hospital.service.SpecialiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/specialite")
public class SpecialiteController {
    @Autowired
    private SpecialiteService specialiteService;

    @Autowired
    private IServiceService serviceService;

    //Liste
    @PreAuthorize("hasAuthority('ROLE_MEDECIN')")
    @GetMapping("/all")
    public @ResponseBody
    List<Specialite> list(){
        return specialiteService.liste();
    }


    //Ajout
    @PreAuthorize("hasAuthority('ROLE_SECRETAIRE') or hasAuthority('ROLE_MEDECIN')")
    @PostMapping(value = "/add" , consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Specialite add(@RequestBody Specialite specialite){
        if(specialite.getService()!=null){
            //On vérifie si le service existe
            Service s = serviceService.findById(specialite.getService().getId());
            if(s!=null) {
                //On ajoute
                specialite.setService(s);
                return specialiteService.addSpecialite(specialite);
            }else{
                return null;
            }
        }else {
            return null;
        }
    }

    @PreAuthorize("hasAuthority('ROLE_SECRETAIRE') or hasAuthority('ROLE_MEDECIN')")
    @PostMapping(value = "/update" , consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Specialite update(@RequestBody Specialite specialite){
        //Vérification
        if(specialite.getService() ==null){
           return null;
        }else {
            Service s = serviceService.findById(specialite.getService().getId());
            if(s!=null) {
                //On met à jour
                specialite.setService(s);
                return specialiteService.addSpecialite(specialite);
            }else{
                return null;
            }
        }

    }


    //Suppression
    @PreAuthorize("hasAuthority('ROLE_SECRETAIRE') or hasAuthority('ROLE_MEDECIN')")
    @PostMapping(value = "/delete" , consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<Specialite> DeleteSpecialite(@RequestBody Specialite specialite){
        specialiteService.DeleteSpecialite(specialite.getId());
        return specialiteService.liste();
    }

}
