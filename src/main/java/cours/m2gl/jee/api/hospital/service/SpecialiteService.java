package cours.m2gl.jee.api.hospital.service;

import cours.m2gl.jee.api.hospital.dao.SpecialiteRepository;
import cours.m2gl.jee.api.hospital.model.Specialite;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@org.springframework.stereotype.Service
public class SpecialiteService implements ISpecialiteService {
    @Autowired
    private SpecialiteRepository specialiteRepository;
    @Override
    public Specialite findById(int id) {
        return specialiteRepository.getOne(id);
    }

    //Ajout d'une spécialité
    @Override
    public Specialite addSpecialite(Specialite specialite) {
        try {
            specialiteRepository.save(specialite);
            return specialite;
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            return null;
        }
    }

    //Suppression d'une spécialité
    @Override
    public void DeleteSpecialite(int id) {
        try {
            specialiteRepository.deleteById(id);
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    //Modification
    @Override
    public Specialite UpdateSpecialite(Specialite specialite) {
        try {
            specialiteRepository.save(specialite);
            return specialite;
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            return null;
        }
    }

    //Liste
    @Override
    public List<Specialite> liste() {
        return specialiteRepository.findAll();
    }
}
