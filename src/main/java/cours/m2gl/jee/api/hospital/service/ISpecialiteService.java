package cours.m2gl.jee.api.hospital.service;

import cours.m2gl.jee.api.hospital.model.Specialite;

import java.util.List;

public interface ISpecialiteService {
    //Récupérer Une spécialité à partir de l'id
    public Specialite findById(int id);

    //Ajouter une spécialité
    public Specialite addSpecialite(Specialite specialite);

    //Supprimer une spécialité
    public void DeleteSpecialite(int id);

    //Modifier les informations d'une spécialité
    public Specialite UpdateSpecialite(Specialite specialite);

    //Liste des spécialité
    public List<Specialite> liste();
}
